//
//  TokioViewController.swift
//  ClimaApp
//
//  Created by Wilson Gabriel Ramos Bravo on 29/5/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import UIKit

class TokioViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBAction func acceptButton(_ sender: Any) {
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=f4de43e15150afc652eed8a39cb85bfc"
        
        let url = URL(string: urlString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {       //con el guard se asegura de que exista data, si no existe termina la funcion y sale de la ejecucion
                print("Error No data")
                return
            }
            guard let weatherInfo = try? JSONDecoder().decode(WilsonWeatherInfo.self, from: data)
                else {
                    print("Error decoding Weather")
                    return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
        }
        task.resume()

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//
//  DennisClima.swift
//  ClimaApp
//
//  Created by Dennis Veintimilla on 30/5/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import Foundation

/*
 "main_weather": "rain"
 
 */

struct DennisWeatherInfo:Decodable{
    let weather: [DennisWeather]
    
}

struct DennisWeather: Decodable{
    let id:Int
    let description:String
}

//
//  SantaMonicaViewController.swift
//  ClimaApp
//
//  Created by Dennis Veintimilla on 29/5/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import UIKit

class SantaMonicaViewController: UIViewController {

  
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func aceptarButton(_ sender: Any) {
            
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=7d484af9a070c648192997831b8cf09a"
        
        let url = URL(string: urlString)
        //url, sesion (informacion del request y response) y
        //qué tarea voy a realizar
        
        let session = URLSession.shared//Singleton, siempre
        //dentro del teléfono
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let weatherInfo = try?
                JSONDecoder().decode(DennisWeatherInfo.self, from: data) else{
                    print("Error decoding Weather")
                    return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            
        }
        task.resume(); //Se manda a ejecutar la tarea (consulta)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

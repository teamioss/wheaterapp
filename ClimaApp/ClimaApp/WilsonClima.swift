//
//  WilsonClima.swift
//  ClimaApp
//
//  Created by Wilson Gabriel Ramos Bravo on 30/5/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import Foundation
/*
 "main_weather": "rain"
 */
struct WilsonWeatherInfo: Decodable {
    let weather: [WilsonWeather]
    
    //let mainWeather: String
    /*
     enum CodingKeys: String, CodingKey {
     case weather
     case mainWeather = "main_weather"
     */
}

struct  WilsonWeather: Decodable{
    let id: Int
    let description: String
    
}
